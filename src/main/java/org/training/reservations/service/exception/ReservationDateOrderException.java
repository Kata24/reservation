package org.training.reservations.service.exception;

@SuppressWarnings("serial")
public class ReservationDateOrderException extends ReservationException {

    public ReservationDateOrderException(String s) {
        super(s);
    }

}
