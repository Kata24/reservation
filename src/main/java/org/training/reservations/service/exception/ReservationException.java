package org.training.reservations.service.exception;

@SuppressWarnings("serial")
public class ReservationException extends Exception {

    public ReservationException(String s) {
        super(s);
    }
}
