package org.training.reservations.service.exception;

@SuppressWarnings("serial")
public class ResourceUnavailableException extends ReservationException {

    public ResourceUnavailableException(String s) {
        super(s);
    }

}
